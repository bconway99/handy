# Handy

A useful little Swift package containing all of those handy little utility methods that no one needs to rewrite in their source. The package contains all manner of extensions and helper classes and will be expanded over the future.
