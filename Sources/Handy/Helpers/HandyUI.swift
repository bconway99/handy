//
//  HandyUI.swift
//  
//
//  Created by Ben Conway on 3/29/20.
//

#if !os(macOS)
// UIKit is not available for MacOS.
import UIKit

public class HandyUI: NSObject {
    
    /// Returns the estimated height for a passed in string value.
    /// - Parameters:
    ///   - str: The length of the string will help estimate the size.
    ///   - font: The size and style of the font will help estimate the size.
    ///   - width: The width of the string container, for example the label of text view width.
    public static func heightForLabel(str: String, font: UIFont, width: CGFloat) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = str
        label.sizeToFit()
        return label.frame.height
    }
    
    /// Returns the estimated width for a passed in string value.
    /// - Parameters:
    ///   - str: The length of the string will help estimate the size.
    ///   - font: The size and style of the font will help estimate the size.
    ///   - height: The height of the string container, for example the label of text view height.
    public static func widthForLabel(str: String, font: UIFont, height: CGFloat) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = str
        label.sizeToFit()
        return label.frame.width
    }
}
#endif
