//
//  HandyNetworking.swift
//  
//
//  Created by Ben Conway on 3/29/20.
//

import Foundation
import SystemConfiguration

public class HandyNetworking: NSObject {
    
    /// Creates the final URL path including the transfer protocol, domain, endpoint and query parameters.
    /// - Parameter domain: The domain of the request URL, which the endpoint will be appended to.
    /// - Parameter endpoint: The endpoint of the request URL also containing the query parameters.
    /// - Parameter isSecure: Determines if the request URL will be prefixed with `https` or `http`.
    public static func createPath(domain: String, endpoint: String, isSecure: Bool) -> String {
        var url = isSecure ? "https" : "http"
        url.append("://")
        let path = String(format: domain, endpoint)
        url.append(path)
        return url
    }
    
    /// Returns a boolean depending on if the device has a network connection or not.
    public static func isConnected() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
