//
//  StringExtension.swift
//  
//
//  Created by Ben Conway on 3/29/20.
//

import Foundation

extension String {
    
    /// Returns the string with only capitalizing the first letter of the first word.
    /// Whereas the the conventional capitalize method capitalizes every word.
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    /// Evaluates a string and returns a boolean if it's a valid email or not.
    public func isValidEmail() -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: self)
    }
}
