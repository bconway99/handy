//
//  DictionaryExtension.swift
//
//
//  Created by Ben Conway on 3/28/20.
//

import Foundation

extension Dictionary {
    
    /// Converts a `JSON file` into a `JSON object`.
    /// - Parameter fileName: The name of the JSON file only, `NOT` the file path.
    public func readJSONFromFile(fileName: String) -> [String : Any]? {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else {
            return nil
        }
        
        do {
            
            let fileUrl = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            let json = try? JSONSerialization.jsonObject(with: data)
            guard let jsonUnwrapped = json as? [String : Any] else {
                return nil
            }
            return jsonUnwrapped
            
        } catch let error {
            
            print("Error: \(error.localizedDescription)")
            return nil
        }
    }
}
