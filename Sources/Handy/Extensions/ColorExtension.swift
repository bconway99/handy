//
//  ColorExtension.swift
//
//
//  Created by Ben Conway on 3/28/20.
//

#if !os(macOS)
// UIKit is not available for MacOS.
import UIKit

// MARK: - Hex Code

extension UIColor {
    
    /// Converts a hex color string into the RGB format that UIColor requires.
    /// All  hex codes must comprise of an 8 digit string, with the first 6 digits being the color and the last 2 digits being the alpha.
    /// - Parameter hex: The hex string consisting of a hash tag and subsequent 8 digits
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

// MARK: - Random

extension UIColor {
    
    /// Assigns a random colour.
    open class var random: UIColor {
        // Get random float values between 0 and 255 for each color.
        let red = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let green = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let blue = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let color = UIColor(red: red,green: green, blue: blue, alpha: 1.0)
        return color
    }
}
#endif
