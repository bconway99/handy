//
//  DateFormatterExtension.swift
//
//
//  Created by Ben Conway on 3/28/20.
//

import Foundation

extension DateFormatter {
    
    static let yyyyMMddHHmmss: String = "yyyy-MM-dd HH:mm:ss"
    
    /// Parse a string into a date object using the appropriate date format.
    /// - Parameter string: The date string to convert into a date object.
    /// - Parameter format: The date format to convert to and from a string.
    public func parse(from string: String, format: String) -> Date? {
        // Set the DateFormatter time zone to be the user's current.
        timeZone = TimeZone.current
        // Set the passed in date string format of the DateFormatter.
        dateFormat = format
        // Convert the String object to a Date object.
        // Either return the unwrapped Date object or nil.
        // Depending on the success or failure of the conversion.
        guard let theDate = date(from: string) else {
            return nil
        }
        return theDate
    }
}
