import XCTest

import HandyTests

var tests = [XCTestCaseEntry]()
tests += HandyTests.allTests()
XCTMain(tests)
