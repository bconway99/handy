import XCTest
@testable import Handy

final class HandyTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        //XCTAssertEqual(Handy().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
